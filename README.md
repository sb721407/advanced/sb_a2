## SkillBox DevOps Advanced. Развертывание кластера Consul.

SkillBox DevOps Advanced. Развертывание кластера Consul.

Для развертывания проекта запустите 

terraform init
terraform apply -auto-approve

Будет создана вся необходимая инфраструктура

Для удаления проекта запустите 

terraform destroy -auto-approve

--------------------------------------------------------

Как работать с репозиторием
```
cd existing_repo
git remote add origin https://gitlab.com/group11618/sb_a2.git
git branch -M main
git push -uf origin main
```

