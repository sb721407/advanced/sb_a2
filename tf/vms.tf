##### VMs Part #####

resource "yandex_compute_instance" "ubuntu18-1" {
  name        = "ubuntu18-1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "ubuntu18-2" {
  name        = "ubuntu18-2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "ubuntu18-3" {
  name        = "ubuntu18-3"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "lemp-1" {
  name        = "lemp-1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87697emk1o3pa6iuva" # lemp
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "lemp-2" {
  name        = "lemp-2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87697emk1o3pa6iuva" # lemp
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "lemp-3" {
  name        = "lemp-3"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd87697emk1o3pa6iuva" # lemp
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}


##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[server]
${yandex_compute_instance.ubuntu18-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.ubuntu18-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.ubuntu18-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[client]
${yandex_compute_instance.lemp-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.lemp-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[template]
${yandex_compute_instance.lemp-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[consul_instances]
${yandex_compute_instance.ubuntu18-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.ubuntu18-1.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.ubuntu18-1.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.ubuntu18-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.ubuntu18-2.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.ubuntu18-2.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.ubuntu18-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.ubuntu18-3.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.ubuntu18-3.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.lemp-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.lemp-1.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.lemp-1.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true
${yandex_compute_instance.lemp-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.lemp-2.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.lemp-2.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true
${yandex_compute_instance.lemp-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.lemp-3.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.lemp-3.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true

EOF
  filename = "${path.module}/inventory"
}

##### Create file inventory 2 #####
resource "local_file" "consul_inv" {
  content  = <<-DOC

[consul_instances]
${yandex_compute_instance.ubuntu18-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.ubuntu18-1.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.ubuntu18-1.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.ubuntu18-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.ubuntu18-2.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.ubuntu18-2.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.ubuntu18-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.ubuntu18-3.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.ubuntu18-3.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.lemp-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.lemp-1.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.lemp-1.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true
${yandex_compute_instance.lemp-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.lemp-2.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.lemp-2.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true
${yandex_compute_instance.lemp-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.lemp-3.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.lemp-3.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true

[client]
${yandex_compute_instance.lemp-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.lemp-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[server]
${yandex_compute_instance.ubuntu18-1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.ubuntu18-2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.ubuntu18-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[template]
${yandex_compute_instance.lemp-3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

    DOC
  filename = "../${path.module}/consul.inv"
}


resource "null_resource" "lemp-1" {
  depends_on = [yandex_compute_instance.lemp-1, local_file.inventory]

  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.lemp-1.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
}

resource "null_resource" "lemp-2" {
  depends_on = [yandex_compute_instance.lemp-2, local_file.inventory]

  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.lemp-2.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${path.module}/test2"
    destination = "/home/ubuntu/test2"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb.yml"
  }
}
